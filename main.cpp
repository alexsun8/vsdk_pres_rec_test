#include "VSDK.h"
#include "darknet_format_to_vsdk.hpp"
#include <string>
#include <CNNDetector.h>
#include <PrecisionRecallMetric.h>
#include <iomanip>
#include "UtilsInner.h"
#include "Points_for_P_R.hpp"

//void pain_img(){}

int main(int argc, char *argv[]) //config, data_path, only_darknet, output, step, n, low_conf
{
    std::string config = argv[1];
    std::string data_path = argv[2];
    bool only_darknet = argv[3];
    std::string output = argv[4];
    int step = std::stoi(argv[5]);
    int n = std::stoi(argv[6]);
    int low_conf = std::stoi(argv[7]);
//    bool gray = argv[7];

    if (only_darknet) {
        darknet_to_Json(data_path);
    }

    Get_Points(data_path, config, output, step, n, low_conf);
//    const std::string inputPath = "/home/alexsun8/streamlabs/data/10/";
//    bool gray = false;

//    Get_Points();
//    Points_to_graf("/home/alexsun8/streamlabs/result/pr_re.txt", "/home/alexsun8/streamlabs/rrr/");
//    std::string path = "/home/alexsun8/streamlabs/result/";
//    auto testMetric = vsdk::IDetectorMetric::create(vsdk::DetectorMetricType::PrecisionRecall);
//    std::string config = "/home/alexsun8/streamlabs/new-new/vsdk/configs/detectors/yolov3-tiny-agemarks/CNNDetector.json";

//    darknet_to_Json(inputPath);
//    vsdk::IDatasetPtr imds = vsdk::IDataset::create(vsdk::DatasetType::Image, inputPath, gray);

//    while (auto item_ptr = imds->next()) {
//        auto testMat = vsdk::GetMat(item_ptr->image);
//        cv::rectangle(testMat,
//                      cv::Rect(item_ptr->markup.value()[0].first.x, item_ptr->markup.value()[0].first.y,
//                               item_ptr->markup.value()[0].first.w, item_ptr->markup.value()[0].first.h),
//                      cv::Scalar(10, 200, 10), 1);
//        cv::imshow("test", testMat);
//        cv::waitKey();
//    }

//    testMetric->calcMetric(vsdk::IDetector::create(vsdk::DetectorType::CNN, config), imds, path,
//                           vsdk::comparator::IoU(0.5));
//    vsdk::IDetectorPtr detector = vsdk::IDetector::create(vsdk::DetectorType::CNN, config);
    //testMetric->calcMetric(detector, imds, "D://", vsdk::comparator::IoU(0.29));

//    LOG(INFO) << "calc Precision-Recall metric for " << detector->name() << " on dataset: " << imds->path();

//    std::vector<vsdk::Detections> vecDet;
//    cv::Mat testMat;
//    cv::Rect testRect;
//    int label;


//    while (auto item_ptr = imds->next()) // while next is working
//    {
////        testMat = vsdk::GetMat(item_ptr->image);
////        testRect = vsdk::ToCvRect(item_ptr->markup.value()[0].first);
////        label = item_ptr->markup.value()[0].second;
////
////        cv::rectangle(testMat, testRect, cv::Scalar(200, 10, 200), 1);
////        cv::imshow("test", testMat);
////        cv::waitKey(0);
//
//        auto detRects = detector->detect(item_ptr->image); // get all rect from detector
//
//        testMat = vsdk::GetMat(item_ptr->image);
////        testRect = vsdk::ToCvRect(detRects[0].rect);
//
//        cv::rectangle(testMat, cv::Rect(detRects[0].rect.x, detRects[0].rect.y, detRects[0].rect.w, detRects[0].rect.h),
//                      cv::Scalar(200, 10, 200), 1);
//        cv::imshow("test", testMat);
//        cv::waitKey(0);
//
//
//        vecDet.push_back(move(detRects));
//    }
//
//    float iouTH1 = 0.5;
////        float iouTH2 = 0.29f;
////        float iouTH3 = 0.39f;
//
//    testMetric->calcMetric(vecDet, imds,
//                           "/home/alexsun8/streamlabs/result/" + std::to_string(iouTH1).substr(0, 4) + "/",
//                           vsdk::comparator::IoU(iouTH1));

//        testMetric->calcMetric(vecDet, imds, "/home/alexd/testing/test_metric/" + std::to_string(iouTH2).substr(0, 4) + "/", vsdk::comparator::IoU(iouTH2));
//
//        testMetric->calcMetric(vecDet, imds, "/home/alexd/testing/test_metric/" + std::to_string(iouTH3).substr(0, 4) + "/", vsdk::comparator::IoU(iouTH3));

    return 0;
}
