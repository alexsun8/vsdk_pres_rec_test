#ifndef VSDK_POINTS_FOR_P_R_HPP
#define VSDK_POINTS_FOR_P_R_HPP

#include "VSDK.h"
#include <filesystem>
#include <iostream>
#include "darknet_format_to_vsdk.hpp"
#include <cmath>


void pres_recall(float conf, const vsdk::IDetectorPtr &detector, vsdk::IDatasetPtr imds, float& prec, float& rec,
            const std::function<float(const vsdk::Rect &rect1, const vsdk::Rect &rect2)> &comp);

void Get_Points(std::string& data_path, std::string& config, std::string& outpath, size_t step, size_t n, size_t start) ;

void Points_to_graf(std::string input, std::string output);

#endif //VSDK_POINTS_FOR_P_R_HPP
