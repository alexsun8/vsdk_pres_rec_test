#include <fstream>
#include <ImagesDataset.h>
#include <Image.h>
#include "darknet_format_to_vsdk.hpp"
#include <algorithm>
#include <iomanip>

#define FOUR(x) x x x x
#define TRIO(x) x x x

bool txt_vs_this_png(std::filesystem::path txt, std::filesystem::path img) {
    std::string txt_filename, img_filename;
    txt_filename = txt.filename();
    img_filename = img.filename();
    size_t i = 0;

    while (txt_filename.back() != '.') {
        txt_filename.pop_back();
    }
    txt_filename.pop_back();

    while (img_filename.back() != '.') {
        img_filename.pop_back();
    }
    img_filename.pop_back();

    if (img_filename.size() != txt_filename.size()) return false;

    for (i = 0; i < img_filename.size(); i++) {
        if (img_filename[i] != txt_filename[i]) return false;
    }
    return true;
}

bool is_txt(std::string &&file) {
    std::string end;
    end += file[file.size() - 3];
    end += file[file.size() - 2];
    end += file[file.size() - 1];

    return !(end != "txt");

}

bool is_img(std::string &&file) {
    std::string end;
    end += file[file.size() - 4];
    end += file[file.size() - 3];
    end += file[file.size() - 2];
    end += file[file.size() - 1];

    return !(end != ".jpg" && end != "jpeg" && end != ".bmp" && end != ".png");

}

bool
getFileContent(std::string fileName,
               std::vector<float> &vec) {

    std::ifstream in(fileName.c_str());

    if (!in) {
        return false;
    }
    std::string str, temp;

    std::getline(in, str);
    size_t cou = 1;
    while (getline(in, temp)) cou++;
    if (cou > 2) return false;

    std::string num;
    for (size_t i = 0; i < str.size(); i++) {
        num.clear();
        while (str[i] != ' ') {
            num += str[i];
            i++;
        }
        if (!num.empty()) {
            vec.push_back(std::stof(num));
        }
    }

    in.close();
    return !(vec.size() != 5);
}


void darknet_to_vsdk(std::string &data_path, std::vector<vsdk::DatasetItem> &img_data, vsdk::Detections &det_ideal,
                     std::vector<std::filesystem::path> &im_path) {

    std::vector<float> vec;
    size_t counter = 0;
    std::filesystem::directory_iterator dit, dit_end;
    dit = std::filesystem::directory_iterator(data_path);

    while (dit != dit_end) {
        vsdk::Detection det;
        vsdk::DatasetItem img;
        if (counter > 5000) return;
        while (!is_img(dit->path().filename().string()) && dit != dit_end) dit++;
        if (dit == dit_end) return;
        std::string p = dit->path().string();
        std::shared_ptr<vsdk::Image> imagePtr = std::make_shared<vsdk::Image>(p, false);
        img.image = imagePtr;

        std::string end;
        end += p[p.size() - 4];
        end += p[p.size() - 3];
        end += p[p.size() - 2];
        end += p[p.size() - 1];

        if (end == "jpeg") {
            FOUR(p.pop_back();)
        } else {
            TRIO(p.pop_back();)
        }
        p += "txt";

        bool same = true;
        if (!getFileContent(p, vec)) same = false;
        int id = (int) vec[0];
        if (same) {
            cv::Mat img_cv = cv::imread(dit->path().string());
            float x = std::max(vec[1] * (float) img_cv.cols, (float) 0),
                    y = std::max(vec[2] * (float) img_cv.rows, (float) 0),
                    w = vec[3] * (float) img_cv.cols,
                    h = vec[4] * (float) img_cv.rows;
            vsdk::Rect rect(x - (w / 2), y - (h / 2), w, h);
            det = {(int) vec[0], rect, 1};

            det_ideal.push_back(det);
            img_data.push_back(img);
            im_path.push_back(dit->path());
            counter++;
//            cv::Mat im = cv::imread(dit->path().string());
//            cv::namedWindow("item", cv::WINDOW_AUTOSIZE);
//            cv::rectangle(im, cv::Rect(det.rect.x, det.rect.y, det.rect.w, det.rect.h),
//                          cv::Scalar(0, 255, 50, 2));
//            cv::imshow("item", im);
//            cv::waitKey();
        }
        vec.clear();
        if (dit != dit_end)dit++;
    }
}

void darknet_to_Json(const std::string &data_path) {
    std::filesystem::directory_iterator dit, dit_end;
    dit = std::filesystem::directory_iterator(data_path);

//    size_t counter =0;

    std::vector<float> rect;
    while (dit != dit_end) {
        while (!is_img(dit->path().filename().string()) && dit != dit_end) {
            dit++;
            if (dit == dit_end) return;
        }
        std::string p = dit->path().string();
        while (p[p.size() - 1] != '.') p.pop_back();
        p += "txt";
        if (getFileContent(p, rect)) {

            cv::Mat img_cv = cv::imread(dit->path().string());
            int x = std::max((int) (rect[1] * (float) img_cv.cols), 0),
                    y = std::max((int) (rect[2] * (float) img_cv.rows), 0),
                    w = (int) (rect[3] * (float) img_cv.cols),
                    h = (int) (rect[4] * (float) img_cv.rows);

            TRIO(p.pop_back();)
            p += "json";

            std::ofstream o(p);
            nlohmann::json j;


            j = {{
                         {"x", x - (w / 2)},
                         {"y", y - (h / 2)},
                         {"w", w},
                         {"h", h},
                         {"label", (int) rect[0]},
                 }};

            o << std::setw(4) << j << std::endl;

            o.close();
            rect.clear();
            dit++;
//            counter++;
        } else {
            std::cout << "p" << std::endl;
        }
//        if(counter%200==0){
//            std::cout<<counter<<std::endl;
//        }
    }
//    std::cout << counter << std::endl;
}

