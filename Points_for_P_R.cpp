#include <fstream>
#include <nlohmann/json.hpp>
#include "Points_for_P_R.hpp"
#include "VSDK.h"
#include <iomanip>


void writeJSON(size_t count, const std::string &path, const std::string &filename) {
    std::string fileName = path + "testMetric.json";
    std::ofstream o(fileName);
    nlohmann::json j;

    j["charts"] = {
            {{"name", "Precision-Recall_curve"}, {"X_axis", "recall"}, {"Y_axis", "precision"}, {"count", count}, {"content", filename}}
    };

    o << std::setw(4) << j << std::endl;
}


void
pres_recall(float conf, const vsdk::IDetectorPtr &detector, vsdk::IDatasetPtr imds, float &prec, float &rec,
            const std::function<float(const vsdk::Rect &rect1, const vsdk::Rect &rect2)> &comp) {
    float TP = 0, FP = 0, FN = 0;
    vsdk::Detections detRects;
    int cl = 1;
    imds->toBegin();
    while (auto item_ptr = imds->next()) {
        detRects = detector->detect(item_ptr->image, vsdk::Rect(), 0);
        bool zero = true;
        int i = 0;
        for (auto item : detRects) {
            if (comp(item.rect, item_ptr->markup.value()[i].first) && zero && cl == item.id &&
                cl == item_ptr.get()->markup.value()[0].second &&
                item.conf >= conf) {
                zero = false;
                TP++;
            } else {
                if (cl == item.id && item.conf >= conf) FP++;
            }
//            cv::Mat im = cv::imread(im_path[i].string());
//            cv::namedWindow("item", cv::WINDOW_AUTOSIZE);
//            cv::rectangle(im, cv::Rect(item.rect.x, item.rect.y, item.rect.w, item.rect.h),
//                          cv::Scalar(255, 0, 50, 2));
//            cv::rectangle(im,
//                          cv::Rect(det_ideal[i].rect.x, det_ideal[i].rect.y, det_ideal[i].rect.w, det_ideal[i].rect.h),
//                          cv::Scalar(0, 255, 50, 2));
//            cv::imshow("item", im);
//            cv::waitKey();
            i++;
        }
        if (zero && item_ptr.get()->markup.value()[0].second == cl)FN++;
        detRects.clear();
    }


    prec = static_cast<float>(TP) / static_cast<float>(TP + FP);
    rec = static_cast<float>(TP) / static_cast<float>(TP + FN);

    std::cout << std::endl << "TP: " << TP << "  FP: " << FP << " FN: " << FN << "  " << "pres = " << prec
              << "  recall = " << rec << std::endl;
}






void Get_Points(std::string& data_path, std::string& config, std::string& outpath, size_t step, size_t n, size_t start) {

    vsdk::IDatasetPtr imds = vsdk::IDataset::create(vsdk::DatasetType::Image, data_path, false);


    vsdk::IDetectorPtr ptr = vsdk::IDetector::create(vsdk::DetectorType::CNN, config);

    std::vector<std::pair<float, float>> pres_recall_vec;

    std::ofstream outMetric(outpath, std::ios::binary);
    size_t size = 0;
    for (size_t i = start, j = 0; i < n; i += step, j++) {
        std::cout << std::endl << ((float) i / n);
        float prec;
        float rec;
        pres_recall((float) i / n, ptr, imds, prec, rec, vsdk::comparator::IoU() = 0.5);
        size++;
        std::cout << prec << "  " << rec << std::endl;

        outMetric.write(reinterpret_cast<char *>(&rec), sizeof(float));
        outMetric.write(reinterpret_cast<char *>(&prec), sizeof(float));
    }
    std::string path = outpath;
    std::string filename = "pres_rec.bin";


    writeJSON(size, path, filename);

}


void Points_to_graf(std::string input, std::string output) {


    std::ifstream in;          // поток для записи
    in.open(input); // окрываем файл для записи

    std::string temp;
    std::vector<std::string> str_vec;

    size_t cou = 1;
    while (getline(in, temp)) {
        str_vec.push_back(temp);
    }
    std::cout << str_vec.size();
    in.close();
    std::ofstream outMetric("/home/alexsun8/streamlabs/rrr/pres_rec.bin", std::ios::binary);
    for (auto str : str_vec) {

        float prec, rec;
        std::string num;
        int i = 0;
        while (str[i] != ' ') {
            num += str[i];
            i++;
        }
        prec = std::stof(num);
        i++;
        num.clear();
        while (str[i] != '\n') {
            num += str[i];
            i++;
        }
        rec = std::stof(num);

        outMetric.write(reinterpret_cast<char *>(&rec), sizeof(float));
        outMetric.write(reinterpret_cast<char *>(&prec), sizeof(float));


    }
    std::string path = "/home/alexsun8/streamlabs/rrr/";
    std::string filename = "pres_rec.bin";


    writeJSON(str_vec.size(), path, filename);
}
