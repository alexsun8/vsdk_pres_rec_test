#ifndef VSDK_DARKNET_FORMAT_TO_VSDK_HPP
#define VSDK_DARKNET_FORMAT_TO_VSDK_HPP

#include <VSDK.h>
#include <filesystem>

bool txt_vs_this_png(std::filesystem::path &txt, std::filesystem::path &img);

bool is_txt(std::string &&file);

bool is_img(std::string &&file);

bool getFileContent(std::string fileName, std::vector<float> &vec);

void darknet_to_vsdk(std::string &data_path, std::vector<vsdk::DatasetItem> &img_data, vsdk::Detections &det_ideal,
                     std::vector<std::filesystem::path> &im_path);

void darknet_to_Json(const std::string &data_path);

#endif //VSDK_DARKNET_FORMAT_TO_VSDK_HPP
